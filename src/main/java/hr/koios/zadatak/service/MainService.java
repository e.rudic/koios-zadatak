package hr.koios.zadatak.service;


import hr.koios.zadatak.model.Article;
import hr.koios.zadatak.model.ArticleDetails;

import java.util.Map;

public interface MainService {

    Map<Article, ArticleDetails> getArticleDetailsMap();
}

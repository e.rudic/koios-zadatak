package hr.koios.zadatak.service.impl;

import hr.koios.zadatak.model.Article;
import hr.koios.zadatak.model.ArticleDetails;
import hr.koios.zadatak.repository.ArticleRepository;
import hr.koios.zadatak.repository.SalesRepository;
import hr.koios.zadatak.repository.StockRepository;
import hr.koios.zadatak.service.MainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class MainServiceImpl implements MainService {
    private final SalesRepository salesRepository;
    private final StockRepository stockRepository;
    private final ArticleRepository articleRepository;

    @Autowired
    public MainServiceImpl(SalesRepository salesRepository, StockRepository stockRepository, ArticleRepository articleRepository) {
        this.salesRepository = salesRepository;
        this.stockRepository = stockRepository;
        this.articleRepository = articleRepository;
    }

    @Override
    public Map<Article, ArticleDetails> getArticleDetailsMap() {
        List<Article> articles = articleRepository.findAll();
        articles.sort(Comparator.comparing(Article::getArticleName));
        Map<Article,ArticleDetails> articleDetailsMap = new TreeMap<>();
        for(Article article: articles){
            ArticleDetails details = new ArticleDetails();
            details.setDaysToOrder(5);
            double average = salesRepository.getSalesAverageFor(article,1);
            details.setAverage(average);
            Optional<Integer> stock = stockRepository.getLatestStock(article,1);
            details.setStock(stock.get());
            articleDetailsMap.put(article,details);
        }
        return articleDetailsMap;
    }
}

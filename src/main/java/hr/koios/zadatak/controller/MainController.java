package hr.koios.zadatak.controller;

import hr.koios.zadatak.model.Article;
import hr.koios.zadatak.model.ArticleDetails;
import hr.koios.zadatak.service.MainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.time.LocalDate;
import java.util.Map;

@Controller
public class MainController {

    private MainService service;

    @Autowired
    public MainController(MainService service){
        this.service=service;
    }

    @GetMapping("/")
    public String home(Model model) {
        int yolo=0;
        Map<Article, ArticleDetails> articleMap = service.getArticleDetailsMap();
        model.addAttribute("map", articleMap);
        model.addAttribute("localDate", LocalDate.now());
        return "home";
    }
}

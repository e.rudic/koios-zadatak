package hr.koios.zadatak.repository;

import hr.koios.zadatak.model.Article;
import hr.koios.zadatak.model.SaleStockId;
import hr.koios.zadatak.model.Sales;
import hr.koios.zadatak.model.Stock;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface StockRepository extends JpaRepository<Stock, SaleStockId> {

    @Query("SELECT stock.stockQuantity FROM Stock stock WHERE stock.date = (SELECT max(s.date) " +
            "FROM Stock s WHERE stock.storeId=s.storeId AND stock.article=s.article" +
            " GROUP BY s.storeId,s.article) AND stock.article=:article AND stock.storeId=:storeId")
    Optional<Integer> getLatestStock(@Param("article") Article article, @Param("storeId") int storeId);
}

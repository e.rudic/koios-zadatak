package hr.koios.zadatak.repository;

import hr.koios.zadatak.model.Article;
import hr.koios.zadatak.model.SaleStockId;
import hr.koios.zadatak.model.Sales;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface SalesRepository extends JpaRepository<Sales, SaleStockId> {

    @Query("SELECT avg(sales.saleCount) FROM Sales sales WHERE sales.storeId=:storeId and sales.article=:article GROUP BY sales.article")
    double getSalesAverageFor(@Param("article")Article article, @Param("storeId") int storeId);
}

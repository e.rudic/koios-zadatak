package hr.koios.zadatak.model;

public class ArticleDetails {
    private double average;
    public int daysToOrder;
    private int stock;

    public ArticleDetails() {
    }

    public ArticleDetails(double average, int daysToOrder) {
        this.average = average;
        this.daysToOrder = daysToOrder;
    }

    public double getAverage() {
        return average;
    }

    public void setAverage(double average) {
        this.average = average;
    }

    public int getDaysToOrder() {
        return daysToOrder;
    }

    public void setDaysToOrder(int daysToOrder) {
        this.daysToOrder = daysToOrder;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }
}

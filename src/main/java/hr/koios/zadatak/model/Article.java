package hr.koios.zadatak.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Article implements Comparable<Article> {

    @Id
    @Column(columnDefinition = "char(2)")
    private String articleName;

    public Article() {
    }

    public String getArticleName() {
        return articleName;
    }

    public void setArticleName(String articleName) {
        this.articleName = articleName;
    }

    @Override
    public int compareTo(Article o) {
        return this.getArticleName().compareTo(o.getArticleName());
    }
}

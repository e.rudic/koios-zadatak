package hr.koios.zadatak.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

public class SaleStockId implements Serializable {
    private LocalDate date;
    private Article article;
    private int storeId;

    public SaleStockId(LocalDate date, Article article, int storeId) {
        this.date = date;
        this.article = article;
        this.storeId = storeId;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }

    public int getStoreId() {
        return storeId;
    }

    public void setStoreId(int storeId) {
        this.storeId = storeId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SaleStockId that = (SaleStockId) o;
        return getStoreId() == that.getStoreId() &&
                getDate().equals(that.getDate()) &&
                getArticle().equals(that.getArticle());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getDate(), getArticle(), getStoreId());
    }
}

package hr.koios.zadatak.model;

import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "SALES")
@IdClass(SaleStockId.class)
public class Sales {
    @Id
    private LocalDate date;

    @Id
    @ManyToOne
    @JoinColumn(name = "article_name")
    private Article article;

    @Id
    private int storeId;

    private int saleCount;

    public Sales(LocalDate date, Article article, int storeId, int saleCount) {
        this.date = date;
        this.article = article;
        this.storeId = storeId;
        this.saleCount = saleCount;
    }

    public Sales() {
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Article getArticleName() {
        return article;
    }

    public void setArticleName(Article article) {
        this.article = article;
    }

    public int getStoreId() {
        return storeId;
    }

    public void setStoreId(int storeId) {
        this.storeId = storeId;
    }

    public int getSaleCount() {
        return saleCount;
    }

    public void setSaleCount(int saleCount) {
        this.saleCount = saleCount;
    }
}

package hr.koios.zadatak.model;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Objects;

@Entity
@Table(name = "STOCK")
@IdClass(SaleStockId.class)
public class Stock {
    @Id
    private LocalDate date;

    @Id
    @ManyToOne
    @JoinColumn(name = "article_name")
    private Article article;

    @Id
    private int storeId;

    private int stockQuantity;

    public Stock(LocalDate date, Article article, int storeId, int stockQuantity) {
        this.date = date;
        this.article = article;
        this.storeId = storeId;
        this.stockQuantity = stockQuantity;
    }

    public Stock() {
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }

    public int getStoreId() {
        return storeId;
    }

    public void setStoreId(int articleId) {
        this.storeId = articleId;
    }

    public int getStockQuantity() {
        return stockQuantity;
    }

    public void setStockQuantity(int stockQuantity) {
        this.stockQuantity = stockQuantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Stock stock = (Stock) o;
        return getStoreId() == stock.getStoreId() &&
                getDate().equals(stock.getDate()) &&
                getArticle().equals(stock.getArticle());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getDate(), getArticle(), getStoreId());
    }
}
